"""
	This file holds the auxiliary functions for the "Sprouts" program
	to keep the main file clean
"""

from Tkinter import *
from classes import *

def sprout_draw_shape(object, mwin):
	if object.__class__.__name__ == "Sprout_Point":
		if object.is_vertex:
			input_list = [object.x-4, object.y-4, object.x+4, object.y+4]
			
			object_id = mwin.window.create_oval(tuple(input_list))
			object.tk_id = object_id
			mwin.window.itemconfig(object.tk_id, fill=object.color)
		else:
			input_list = [object.x, object.y, object.x, object.y]
			
			object_id = mwin.window.create_oval(tuple(input_list))
			object.tk_id = object_id
			mwin.window.itemconfig(object.tk_id, fill="black")
			
	elif object.__class__.__name__ == "Sprout_Line":
		input_list = [ends[0].x, ends[0].y, ends[1].x, ends[1].y ]
		
		object_id = mwin.window.create_line(tuple(input_list))
		object.tk_id = object_id
		mwin.window.itemconfig(object.tk_id, fill=object.color)
	
def sprout_draw_all(mwin):
	for s in mwin.new_items:
		if not s.is_vertex:
			sprout_draw_shape(s, mwin)
	for s in mwin.new_items:
		if s.is_vertex:
			sprout_draw_shape(s, mwin)
			mwin.verticies.append(s)
			if mwin.next_time == 0:
				#print "pass"
				pass
			elif mwin.next_time == 1:
				temp = mwin.verticies[2]
				mwin.verticies[2] = mwin.verticies[3]
				mwin.verticies[3] = temp
				mwin.next_time = 0
				#print "swap1"
			elif mwin.next_time == 2:
				temp = mwin.verticies[4]
				mwin.verticies[4] = mwin.verticies[3]
				mwin.verticies[3] = temp
				mwin.next_time = 0
				#print "swap2"
			elif mwin.next_time == 3:
				temp = mwin.verticies[2]
				mwin.verticies[2] = mwin.verticies[4]
				mwin.verticies[4] = mwin.verticies[3]
				mwin.verticies[3] = temp
				mwin.next_time = 0
				#print "swap3"
			elif mwin.next_time == 4:
				temp = mwin.verticies[2]
				mwin.verticies[2] = mwin.verticies[3]
				mwin.verticies[3] = mwin.verticies[4]
				mwin.verticies[4] = temp
				mwin.next_time = 0
				#print "swap4"
			elif mwin.next_time == 5:
				temp = mwin.verticies[1]
				mwin.verticies[1] = mwin.verticies[3]
				mwin.verticies[3] = mwin.verticies[2]
				mwin.verticies[2] = temp
				mwin.next_time = 0
				#print "swap5"
			elif mwin.next_time == 6:
				temp1 = mwin.verticies[1]
				temp3 = mwin.verticies[3]
				temp4 = mwin.verticies[4]
				mwin.verticies[4] = temp3
				mwin.verticies[3] = temp1
				mwin.verticies[1] = temp4
				mwin.next_time = 0
				#print "swap6"
			elif mwin.next_time == 7:
				temp2 = mwin.verticies[2]
				temp3 = mwin.verticies[3]
				temp4 = mwin.verticies[4]
				mwin.verticies[4] = temp3
				mwin.verticies[3] = temp2
				mwin.verticies[2] = temp4
				mwin.next_time = 0
				#print "swap7"
			elif mwin.next_time == 8:
				temp1 = mwin.verticies[1]
				temp2 = mwin.verticies[2]
				temp3 = mwin.verticies[3]
				temp4 = mwin.verticies[4]
				mwin.verticies[2] = temp1
				mwin.verticies[3] = temp2
				mwin.verticies[4] = temp3
				mwin.verticies[1] = temp4
				mwin.next_time = 0
				#print "swap8"
			elif mwin.next_time == 9:
				temp2 = mwin.verticies[2]
				temp3 = mwin.verticies[3]
				temp4 = mwin.verticies[4]
				mwin.verticies[3] = temp2
				mwin.verticies[4] = temp3
				mwin.verticies[2] = temp4
				mwin.next_time = 0
				#print "swap9"
			
			
			#print "verticies = ", mwin.verticies
	
	mwin.new_items = []

def setup_mouse(mwin):
	def vertex_selector(event):
		clicked_nothing = True
		if mwin.offer_active:
			for p in mwin.o_choices:
				if p.near(event.x, event.y):
					clicked_nothing = False
					mwin.o_chosen = p
					break
			mwin.o_choices = []
			mwin.offer_active = False
					
		else:
			for i in mwin.verticies:
				if not i.is_vertex:
					continue
					
				if i.near(event.x, event.y):
					#print "verticies ", mwin.verticies
					clicked_nothing = False
					if mwin.selected_point1 == None:
						mwin.selected_point1 = i
						mwin.window.itemconfig(i.tk_id, fill="blue")
						i.color = "blue"
					elif mwin.selected_point2 == None:
						mwin.selected_point2 = i
						mwin.window.itemconfig(i.tk_id, fill="green")
						i.color = "green"
					else:
						mwin.window.itemconfig(mwin.selected_point2.tk_id, fill="red")
						mwin.selected_point2.color = "red"
						
						mwin.selected_point2 = i
						mwin.window.itemconfig(i.tk_id, fill="green")
						i.color = "green"
			
			
		if clicked_nothing:	
			if mwin.selected_point1 != None:
				mwin.window.itemconfig(mwin.selected_point1.tk_id, fill="red")
				mwin.selected_point1.color = "red"
				mwin.selected_point1 = None
			
			if mwin.selected_point2 != None:
				mwin.window.itemconfig(mwin.selected_point2.tk_id, fill="red")
				mwin.selected_point2.color = "red"
				mwin.selected_point2 = None
	
	def locate(event):
		mwin.window.itemconfig(mwin.mouse_loc, text="("+str(event.x)+","+str(event.y)+")")
	
	#mwin.window.bind("<Motion>", locate)
	mwin.window.bind("<ButtonPress-1>", vertex_selector)
	#mwin.window.bind("<ButtonRelease-1>", object_releaser)


def connect_points(mwin, point1, point2, type=1, point3 = None):
	#print point1, point2
	if point3 != None:
		point3.make_vertex()
		mwin.item_list.append(point3)
		mwin.new_items.append(point3)
		
		line1 = Sprout_Line(point1, point3)
		line2 = Sprout_Line(point3, point2)
		
		mwin.connections.append([point1, point3])
		mwin.connections.append([point3, point2])
		mwin.go_around.append([point1, point2])	
		mwin.go_around.append([point2, point3])	
		mwin.go_around.append([point1, point3])
		
		lines = []
		lines.append(line1)
		lines.append(line2)
	
	elif point1 == point2:
		loop_size = (mwin.width + mwin.height)/16
		
		point3 = Sprout_Point(point1.x, point1.y - loop_size)
		point4 = Sprout_Point(point1.x + loop_size/3, point1.y - loop_size/2)
		point5 = Sprout_Point(point1.x - loop_size/3, point1.y - loop_size/2)
		
		line1 = Sprout_Line(point1, point4)
		line2 = Sprout_Line(point4, point3)
		line3 = Sprout_Line(point3, point5)
		line4 = Sprout_Line(point5, point1)
		
		flip = False
		for p in mwin.item_list:
			if line1.on_line(p):
				flip = True
			if line2.on_line(p):
				flip = True
			if line3.on_line(p):
				flip = True
			if line4.on_line(p):
				flip = True
			if point3 == p:
				flip = True
		
		if flip:
			point3 = Sprout_Point(point1.x, point1.y + loop_size)
			point4 = Sprout_Point(point1.x + loop_size/3, point1.y + loop_size/2)
			point5 = Sprout_Point(point1.x - loop_size/3, point1.y + loop_size/2)
		
			line1 = Sprout_Line(point1, point4)
			line2 = Sprout_Line(point4, point3)
			line3 = Sprout_Line(point3, point5)
			line4 = Sprout_Line(point5, point1)
		
		
		for p in mwin.item_list:
			if line1.on_line(p):
				print "cannot connect"
				return False
			if line2.on_line(p):
				print "cannot connect"
				return False
			if line3.on_line(p):
				print "cannot connect"
				return False
			if line4.on_line(p):
				print "cannot connect"
				return False
			if point3 == p:
				print "cannot connect"
				return False
		
		lines = []
		lines.append(line1)
		lines.append(line2)
		lines.append(line3)
		lines.append(line4)
		
		point3.make_vertex()
		mwin.item_list.append(point3)
		mwin.new_items.append(point3)
		mwin.connections.append([point1, point3])
		mwin.connections.append([point3, point1])
		
	else:
		temp_distance = (abs(point1.x - point2.x) + abs(point1.y - point2.y))
		
		lines = []
		
		if type == 1:
			point3 = Sprout_Point((point1.x+point2.x)/2, (point1.y+point2.y)/2)
			if point3 in mwin.item_list:
				type = 2	
					
			line1 = Sprout_Line(point1, point3)
			line2 = Sprout_Line(point3, point2)
			
			for p in mwin.item_list:
				if line1.on_line(p):
					type = 2
				if line2.on_line(p):
					type = 2
					
		if type == 2:
			point3 = Sprout_Point((point1.x+point2.x)/2+temp_distance/2, (point1.y+point2.y)/2)
			if point3 in mwin.item_list:
				type = 3
					
			line1 = Sprout_Line(point1, point3)
			line2 = Sprout_Line(point3, point2)
			
			for p in mwin.item_list:
				if line1.on_line(p):
					type = 3
				if line2.on_line(p):
					type = 3
		if type == 3:
			point3 = Sprout_Point((point1.x+point2.x)/2-temp_distance/2, (point1.y+point2.y)/2)
			if point3 in mwin.item_list:
				type = 4
					
			line1 = Sprout_Line(point1, point3)
			line2 = Sprout_Line(point3, point2)
			
			for p in mwin.item_list:
				if line1.on_line(p):
					type = 4
				if line2.on_line(p):
					type = 4
		if type == 4:
			point3 = Sprout_Point((point1.x+point2.x)/2+2*temp_distance/3, (point1.y+point2.y)/2)
			if point3 in mwin.item_list:
				type = 5
					
			line1 = Sprout_Line(point1, point3)
			line2 = Sprout_Line(point3, point2)
			
			for p in mwin.item_list:
				if line1.on_line(p):
					type = 5
				if line2.on_line(p):
					type = 5
		if type == 5:
			point3 = Sprout_Point((point1.x+point2.x)/2-2*temp_distance/3, (point1.y+point2.y)/2)
					
			line1 = Sprout_Line(point1, point3)
			line2 = Sprout_Line(point3, point2)
			
			for p in mwin.item_list:
				if line1.on_line(p):
					type = 6
				if line2.on_line(p):
					type = 6
			
		if type == 6:
			point3 = Sprout_Point((point1.x+point2.x)/2+4*temp_distance/5, (point1.y+point2.y)/2)
					
			line1 = Sprout_Line(point1, point3)
			line2 = Sprout_Line(point3, point2)
			
			for p in mwin.item_list:
				if line1.on_line(p):
					type = -1
				if line2.on_line(p):
					type = -1
			
			if type == -1:
				if mwin.tree[0] == "D" and mwin.tree[1] == 6:
					type = 7
				else:
					type = 8
		if type == 7:
			if mwin.tree[2] == "top":
				point3 = Sprout_Point((point1.x+point2.x)/2+1*temp_distance/3, max(point1.y, point2.y))
			else:
				point3 = Sprout_Point((point1.x+point2.x)/2+1*temp_distance/3, min(point1.y, point2.y))
			
			line1 = Sprout_Line(point1, point3)
			line2 = Sprout_Line(point3, point2)
			
			for p in mwin.item_list:
				if line1.on_line(p):
					type = -1
				if line2.on_line(p):
					type = -1
			
			if type == -1:
				type = 12
		if type == 8:
			point3 = Sprout_Point((point1.x+point2.x)/2-4*temp_distance/5, (point1.y+point2.y)/2)
					
			line1 = Sprout_Line(point1, point3)
			line2 = Sprout_Line(point3, point2)
			
			for p in mwin.item_list:
				if line1.on_line(p):
					type = -1
				if line2.on_line(p):
					type = -1
			
			if type == -1:
				if mwin.tree[0] == "E" and mwin.tree[1] == 13:
					type = 9
				elif mwin.tree[0] == "D" and mwin.tree[1] == 9:
					type = 10
				elif mwin.tree[0] == "E" and mwin.tree[1] == 1:
					type = 11
				elif mwin.tree[0] == "E" and mwin.tree[1] == 6 or\
				mwin.tree[0] == "E" and mwin.tree[1] == 9:
					type = 12
				elif mwin.tree[0] == "E" and mwin.tree[1] == 10:
					type = 13
				elif mwin.tree[0] == "E" and mwin.tree[1] == 18:
					type = 14
				else:
					print "cannot connect"
					return False
		if type == 9:
			if mwin.tree[2] == "top":
				point3 = Sprout_Point((point1.x+point2.x)/2+50, mwin.height - 100)
			else:
				point3 = Sprout_Point((point1.x+point2.x)/2+50, 100)
			
			
			line1 = Sprout_Line(point1, point3)
			line2 = Sprout_Line(point3, point2)
			
			"""for p in mwin.item_list:
				if line1.on_line(p):
					type = -1
				if line2.on_line(p):
					type = -1
			"""
			if type == -1:
				print "cannot connect"
				return False
		if type == 10:
			point3 = Sprout_Point(500, mwin.height - 50)
					
			line1 = Sprout_Line(point1, point3)
			line2 = Sprout_Line(point3, point2)
			
			for p in mwin.item_list:
				if line1.on_line(p):
					type = -1
				if line2.on_line(p):
					type = -1
			
			if type == -1:
				print "cannot connect"
				return False
		if type == 11:
			if mwin.tree[2] == "top":
				point3 = Sprout_Point(600, 400)
			else:
				point3 = Sprout_Point(555, 180)
			
								
			line1 = Sprout_Line(point1, point3)
			line2 = Sprout_Line(point3, point2)
			
			for p in mwin.item_list:
				if line1.on_line(p):
					type = -1
				if line2.on_line(p):
					type = -1
			
			if type == -1:
				print "cannot connect"
				return False
		if type == 12:
			point3 = Sprout_Point((point1.x+point2.x)/2, (point1.y+point2.y)/2)
			
			line1 = Sprout_Line(point1, point3)
			line2 = Sprout_Line(point3, point2)
		if type == 13:
			if mwin.tree[2] == "bot":
				point3 = Sprout_Point(445, 100)
			else:
				point3 = Sprout_Point(400, 500)				
			
			line1 = Sprout_Line(point1, point3)
			line2 = Sprout_Line(point3, point2)
		if type == 14:
			point3 = Sprout_Point(475, 500)				
			
			line1 = Sprout_Line(point1, point3)
			line2 = Sprout_Line(point3, point2)
			
		
		point3.make_vertex()
		mwin.item_list.append(point3)
		mwin.new_items.append(point3)
		mwin.connections.append([point1, point3])
		mwin.connections.append([point3, point2])
		mwin.go_around.append([point1, point2])	
		mwin.go_around.append([point2, point3])	
		mwin.go_around.append([point1, point3])
		lines.append(line1)
		lines.append(line2)
	
	for line in lines:
		temp_loc = [line.ends[0].x, line.ends[0].y]
		while True:
			temp_point = Sprout_Point(temp_loc[0], temp_loc[1])
			#line.points.append(temp_point)
			mwin.item_list.append(temp_point)
			mwin.new_items.append(temp_point)
			
			if temp_point.x == line.ends[1].x and  temp_point.y == line.ends[1].y:
				break
			
			if temp_point.y + 1 < mwin.height:
				temp_point2 = Sprout_Point(temp_loc[0], temp_loc[1] + 1)
				#line.points.append(temp_point2)
				mwin.item_list.append(temp_point2)
				mwin.new_items.append(temp_point2)
				
			if line.slope == None or line.y_int == None: # vertical line
				if line.ends[1].x > temp_loc[0]:
					temp_loc[0] += 1
				elif line.ends[1].x < temp_loc[0]:
					temp_loc[0] -= 1
				
				if line.ends[1].y > temp_loc[1]:
					temp_loc[1] += 1
				elif line.ends[1].y < temp_loc[1]:
					temp_loc[1] -= 1
					
			else:
				if temp_loc[1] > line.slope*temp_loc[0] + line.y_int:  # too "high"
					if line.slope > 0:
						if line.ends[1].x > temp_loc[0]:
							temp_loc[0] += 1
						elif line.ends[1].x < temp_loc[0]:
							temp_loc[0] -= 0
					elif line.slope < 0:
						if line.ends[1].x > temp_loc[0]:
							temp_loc[0] += 0
						elif line.ends[1].x < temp_loc[0]:
							temp_loc[0] -= 1
					else:
						if line.ends[1].x > temp_loc[0]:
							temp_loc[0] += 1
						elif line.ends[1].x < temp_loc[0]:
							temp_loc[0] -= 1
					
					if line.ends[1].y > temp_loc[1]:
						temp_loc[1] += 0
					elif line.ends[1].y < temp_loc[1]:
						temp_loc[1] -= 1
						
				elif temp_loc[1] < line.slope*temp_loc[0] + line.y_int:  # too "low"
					if line.slope > 0:
						if line.ends[1].x > temp_loc[0]:
							temp_loc[0] += 0
						elif line.ends[1].x < temp_loc[0]:
							temp_loc[0] -= 1
					elif line.slope < 0:
						if line.ends[1].x > temp_loc[0]:
							temp_loc[0] += 1
						elif line.ends[1].x < temp_loc[0]:
							temp_loc[0] -= 0
					else:
						if line.ends[1].x > temp_loc[0]:
							temp_loc[0] += 1
						elif line.ends[1].x < temp_loc[0]:
							temp_loc[0] -= 1
					
					if line.ends[1].y > temp_loc[1]:
						temp_loc[1] += 1
					elif line.ends[1].y < temp_loc[1]:
						temp_loc[1] -= 0
				else:
					if line.ends[1].x > temp_loc[0]:
						temp_loc[0] += 1
					elif line.ends[1].x < temp_loc[0]:
						temp_loc[0] -= 1
					
					if line.ends[1].y > temp_loc[1]:
						temp_loc[1] += 1
					elif line.ends[1].y < temp_loc[1]:
						temp_loc[1] -= 1
	
	return point3		

def update_level(mwin, point1, point2, point3 = None):
	
	loop_left = 470
	loop_right = 535
	
	if mwin.tree[0] == "A":
		if point1 == point2:
			if point1 == mwin.verticies[0]:
				mwin.tree[1] = 1
				mwin.tree[2] = "top"
			else:
				mwin.tree[1] = 1
				mwin.tree[2] = "bot"
		else:
			mwin.tree[1] = 2
	elif mwin.tree[0] == "B":
		if mwin.tree[1] == 1:
			if point3 != None:
				if point3.x > loop_left and point3.x < loop_right:
					mwin.tree[1] = 3
				else:
					mwin.tree[1] = 1
			else:
				if point1 == point2:
					mwin.tree[1] = 2
					if mwin.tree[2] == "bot":
						mwin.next_time = 1
						mwin.tree[2] = "top"
				else:
					mwin.tree[1] = 4
		else:
			if point1 == point2:
				mwin.tree[1] = 4
				if point1 == mwin.verticies[0]:
					mwin.tree[2] = "top"
					mwin.next_time = 1
				elif point1 == mwin.verticies[1]:
					mwin.tree[2] = "bot"
					mwin.next_time = 5
			elif point1 == mwin.verticies[0] and point2 == mwin.verticies[1] or\
			point1 == mwin.verticies[1] and point2 == mwin.verticies[0]:
			 	mwin.tree[1] = 6 	
			else:
				mwin.tree[1] = 5
				if point1 == mwin.verticies[0] or point2 == mwin.verticies[0]:
					mwin.tree[2] = "top"
				else:
					mwin.tree[2] = "bot"
	elif mwin.tree[0] == "C":
		if mwin.tree[1] == 1:
			if point1 == point2:
				mwin.tree[1] = 2
			else:
				mwin.tree[1] = 1
		elif mwin.tree[1] == 2:
			if point3 != None:
				if point3.x > loop_left and point3.x < loop_right:
					mwin.tree[1] = 3
				else:
					mwin.tree[1] = 2
				if point1 == mwin.verticies[0] or point2 == mwin.verticies[0]:
					mwin.tree[2] = "top"
					mwin.next_time = 2
				else:
					mwin.tree[2] = "bot"
					mwin.next_time = 4
			else:
				mwin.tree[1] = 4
		elif mwin.tree[1] == 3:
			mwin.tree[1] = 3
			if mwin.tree[2] == "top":
				mwin.next_time = 2
			else:
				mwin.next_time = 3
		elif mwin.tree[1] == 4:
			if point1 == point2:
				mwin.tree[1] = 4
				if mwin.tree[2] == "top":
					mwin.next_time = 6
				else:
					mwin.next_time = 7
			elif mwin.tree[2] == "top":
				if point1 == mwin.verticies[2] and point2 == mwin.verticies[3] or\
				point1 == mwin.verticies[3] and point2 == mwin.verticies[2]:
					mwin.tree[1] = 5
				elif point1 == mwin.verticies[2] and point2 == mwin.verticies[1] or\
				point1 == mwin.verticies[1] and point2 == mwin.verticies[2]:
					mwin.tree[1] = 6
				else:	
					mwin.tree[1] = 7
			else:
				if point1 == mwin.verticies[1] and point2 == mwin.verticies[3] or\
				point1 == mwin.verticies[3] and point2 == mwin.verticies[1]:
					mwin.tree[1] = 5
				elif point1 == mwin.verticies[1] and point2 == mwin.verticies[0] or\
				point1 == mwin.verticies[0] and point2 == mwin.verticies[1]:
					mwin.tree[1] = 6
				else:	
					mwin.tree[1] = 7
		elif mwin.tree[1] == 5:
			if point1 == point2:
				mwin.tree[1] = 7
				if mwin.tree[2] == "top":
					mwin.tree[2] = "bot"
					mwin.next_time = 8
				else:
					mwin.tree[2] = "top"
					mwin.next_time = 9
			else:
				mwin.tree[1] = 8
		else:
			if point1 == mwin.verticies[0] and point2 == mwin.verticies[1] or\
			point1 == mwin.verticies[1] and point2 == mwin.verticies[0]:
				mwin.tree[1] = 9
				mwin.tree[2] = "top"
			elif point1 == mwin.verticies[2] and point2 == mwin.verticies[3] or\
			point1 == mwin.verticies[3] and point2 == mwin.verticies[2]:
				mwin.tree[1] = 9
				mwin.tree[2] = "bot"
			else:
				mwin.tree[1] = 10
	elif mwin.tree[0] == "D":
		if mwin.tree[1] == 1:
			if point1 == point2:
				mwin.tree[1] = 2
			else:
				mwin.tree[1] = 1
		elif mwin.tree[1] == 2:
			if point3 != None:
				if point3.x > loop_left and point3.x < loop_right:
					mwin.tree[1] = 4
				else:
					mwin.tree[1] = 3
			else:
				mwin.tree[1] = 2
		elif mwin.tree[1] == 3:
			if point3.x > loop_left and point3.x < loop_right:
				mwin.tree[1] = 5
			else:
				mwin.tree[1] = 4
				
			mwin.game_over = True
		elif mwin.tree[1] == 4:
			if point1 == mwin.verticies[4] or point2 == mwin.verticies[4]:
				mwin.tree[1] = 7
			else:
				mwin.tree[1] = 6
		elif mwin.tree[1] == 5:
			if point1 == point2:
				mwin.tree[1] = 7
			else:
				mwin.tree[1] = 8
		elif mwin.tree[1] == 6:
			if point1 == mwin.verticies[4] and point2 == mwin.verticies[3] or\
			point1 == mwin.verticies[3] and point2 == mwin.verticies[4]:
				mwin.tree[1] = 9
			else:
				mwin.tree[1] = 10
		elif mwin.tree[1] == 7:
			if point3 == None:
				mwin.tree[1] = 10
			else:
				if point3.x > 500 and point3.x < loop_right:
					mwin.tree[1] = 12
					mwin.game_over = True
				else:
					mwin.tree[1] = 11
		elif mwin.tree[1] == 8:
			if point3 == None:
				mwin.tree[1] = 13
			else:
				if point3.x > 450:
					mwin.tree[1] = 15
					mwin.game_over = True
				else:
					mwin.tree[1] = 14
		elif mwin.tree[1] == 9:	
			if mwin.tree[2] == "bot":
				if point1 == mwin.verticies[4] or point2 == mwin.verticies[4]:
					mwin.tree[1] = 17
					mwin.game_over = True
				else:
					mwin.tree[1] = 16
					mwin.game_over = True
			else:
				if point1 == mwin.verticies[2] or point2 == mwin.verticies[2]:
					mwin.tree[1] = 17
					mwin.game_over = True
				else:
					mwin.tree[1] = 16
					mwin.game_over = True
		elif mwin.tree[1] == 10:
			if point3 == None:
				mwin.tree[1] = 18
			else:
				if point3.y < 400:
					mwin.tree[1] = 20
					mwin.game_over = True
				else:
					mwin.tree[1] = 19
	elif mwin.tree[0] == "E":
		mwin.game_over = True
			
			
			
			
	if mwin.tree[0] == "A":
		mwin.tree[0] = "B"
	elif mwin.tree[0] == "B":
		mwin.tree[0] = "C"
	elif mwin.tree[0] == "C":
		mwin.tree[0] = "D"
	elif mwin.tree[0] == "D":
		mwin.tree[0] = "E"
	elif mwin.tree[0] == "E":
		mwin.tree[0] = "F"
	
	mwin.window.itemconfig(mwin.tree_text, text=mwin.tree[0]+" "
								+ str(mwin.tree[1])+" "+mwin.tree[2])
	
	if mwin.player1:
		mwin.player1 = False
		if mwin.computer:
			mwin.window.itemconfig(mwin.player_text, text="Computer")
		else:
			mwin.window.itemconfig(mwin.player_text, text="Player 2")
	else:
		mwin.player1 = True
		if mwin.computer:
			mwin.window.itemconfig(mwin.player_text, text="Player")
		else:
			mwin.window.itemconfig(mwin.player_text, text="Player 1")
		
	
	if mwin.game_over:
		if mwin.tree[0] == "E":
			if mwin.computer == True:
				mwin.window.create_text(mwin.width/2, 50, 
									text="Computer Wins!", font= ("arial", 32, "bold"), fill="darkgreen")
			else:
				mwin.window.create_text(mwin.width/2, 50, 
									text="Player 2 Wins!", font= ("arial", 32, "bold"), fill="darkgreen")
				
		else:
			mwin.window.create_text(mwin.width/2, 50, 
									text="Player 1 Wins!", font= ("arial", 32, "bold"), fill="darkgreen")
		
			
	
			
			
			
			
			
			
			

