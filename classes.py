#	This file holds all classes used in "Sprouts" Program

from Tkinter import *
import time

class Sprout_Canvas:
	def __init__(self, width=1000, height=600):
		self.width = width
		self.height = height
		
		# Set up canvas
		self.master = Tk()
		self.window = Canvas(self.master, width=self.width, height=self.height)
		self.master.title("Sprouts")
		self.window.pack()
		self.window.config(bg = "#%02x%02x%02x" % (245,235,215))
		
		self.selected_point1 = None
		self.selected_point2 = None
		self.item_list = []
		self.new_items = []
		self.connections = []
		self.go_around = []
		self.verticies = []
		
		# Offer
		self.offer_active = False
		self.o_choices = []
		self.o_chosen = None
		self.next_time = 0
		
		
		# AI
		self.game_tree = Game_Tree()
		self.player1 = True
		self.game_over = False
		self.computer = True
		self.player_text = self.window.create_text(50, 20, 
									text="Player", font= ("arial", 12, "bold"))
		
		# mouse location
		#self.mouse_loc = self.window.create_text(50, 20, 
		#							text="(0,0)", font= ("arial", 12, "bold"))
		
		# game tree
		self.tree = ["A", 1, "top"] # level, place in level
		self.tree_text = self.window.create_text(self.width - 40, 20, 
							text="A 1 top", font= ("arial", 12, "bold"), fill="dark blue")

class Sprout_Point:
	def __init__(self, init_x, init_y):
		self.x = init_x
		self.y = init_y
		self.color = ""
		self.tk_id = None
		self.is_vertex = False
	
	def near(self, in_x, in_y):
		if (abs(in_x - self.x) + abs(in_y - self.y)) < 9:
			return True
		else:
			return False
	
	def make_vertex(self):
		self.is_vertex = True
		self.color = "red"
	
	def __eq__(self, other):
		if other == None:
			return False
		
		if(self.x == other.x and self.y == other.y):
			return True
		else:
			return False
	
	def __str__(self):
		return "("+ str(self.x) +", "+ str(self.y) +")"
	
	def __repr__(self):
		return self.__str__()
	
	def clone(self):
		clone = Sprout_Point(self.x, self.y)
		clone.color = self.color
		clone.tk_id = self.tk_id
		clone.is_vertex = self.is_vertex


class Sprout_Line:
	def __init__(self, point_1, point_2, type = 1):
		self.ends = [point_1, point_2]
		self.color = ""
		#type is 1, 2, or 3 for straight, curve right, or curve left
		self.line_type = type
		self.points = []
		
		if point_1.x == point_2.x: # vertical line
			self.slope = None
			self.y_int = None
		else:
			self.slope = (point_1.y - point_2.y)*1.0/(point_1.x - point_2.x)
			self.y_int = point_1.y - self.slope*point_1.x
		
		self.tk_id = None
	
	def change_points(self, point_1, point_2):
		self.points = [point_1, point_2]
	
	def near_shape(self, x, y):
		return False
	
	def on_line(self, point):
		if self.slope == None:
			if point.x == self.ends[0].x:
				if (point.y > min(self.ends[0].y, self.ends[1].y) + 10) and\
					(point.y < max(self.ends[0].y, self.ends[1].y) - 10):
					return True
				else:
					return False
			else:
				return False
		if ((point.y > min(self.ends[0].y, self.ends[1].y) + 10) and\
				(point.y < max(self.ends[0].y, self.ends[1].y) - 10)) or\
				((point.x > min(self.ends[0].x, self.ends[1].x) + 10) and\
				(point.x < max(self.ends[0].x, self.ends[1].x) - 10)):
				
			if abs (self.slope*point.x + self.y_int - point.y) < 5:
				return True
			else:
				return False
		else:
			return False
	

class Game_Tree:
	def __init__(self):
		self.nodes = {}
		self.nodes["A 1 top"] = [0, 1, None]
		
		self.nodes["B 1 top"] = [0, 2, Sprout_Point(500, 150)]
		self.nodes["B 1 bot"] = [1, 2, Sprout_Point(500, 350)]
		self.nodes["B 2 top"] = [0, 1, None]
		
		self.nodes["D 3 top"] = [1, 3, Sprout_Point(500, 350)]
		self.nodes["D 3 bot"] = [0, 2, Sprout_Point(500, 150)]
		self.nodes["D 9 top"] = [3, 4, None]
		self.nodes["D 9 bot"] = [0, 1, None]
		self.nodes["D 10 top"] = [1, 3, Sprout_Point(530, 321)]
	
	def play(self, level, place, orient):
		return self.nodes[level+" "+str(place)+" "+orient]
	
	

