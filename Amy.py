"""
	This is a Python implementation of the game "Sprouts"
	Written by Samuel Inniss
	
	cd ~/Google\ Drive/Summer2015/AI/Sprouts/Sprouts/
	python Amy.py
"""

from classes import *
from helpers import *
import sys
import multiprocessing

mwin = Sprout_Canvas()

try:
	pvc = sys.argv[1]
	if pvc[0] == 'c' or pvc[2] == 'C':
		mwin.computer = True
	else:
		mwin.computer = False
		try:
			if pvc[2] == 'c' or pvc[2] == 'C':
				mwin.computer = True
		except:
			pass
except:
	mwin.computer = False

if mwin.computer == False:
	mwin.window.itemconfig(mwin.player_text, text="Player 1")

refresh_time = time.time()

# Initial dots
point1 = Sprout_Point(mwin.width/2, mwin.height*1/3)
point1.make_vertex()
mwin.item_list.append(point1)

point2 = Sprout_Point(mwin.width*2/4, mwin.height*2/3)
point2.make_vertex()
mwin.item_list.append(point2)

mwin.new_items = mwin.item_list

setup_mouse(mwin)


def main():
	global refresh_time
		
	while True:
		if (time.time() - refresh_time > 1/10):
			refresh_time = time.time()
			
			if mwin.computer == True and mwin.player1 == False:
				move = mwin.game_tree.play(mwin.tree[0], mwin.tree[1], mwin.tree[2])
				temp_point1 = mwin.verticies[move[0]]
				temp_point2 = mwin.verticies[move[1]]
				
				mwin.selected_point1 = temp_point1
				mwin.selected_point2 = temp_point2
				if move[2] != None:
					mwin.o_chosen = move[2]
					new_vertex = connect_points(mwin, temp_point1, temp_point2, 1, mwin.o_chosen)
					print "Computer played ", mwin.verticies[move[0]], mwin.verticies[move[1]], move[2]
				
				else:
					new_vertex = connect_points(mwin, temp_point1, temp_point2, 1)
					print "Computer played ", mwin.verticies[move[0]], mwin.verticies[move[1]], " None"
				
				update_level(mwin, temp_point1, temp_point2, mwin.o_chosen)
				
				mwin.o_chosen = None
				
				mwin.window.itemconfig(temp_point1.tk_id, fill="red")
				temp_point1.color = "red"
				mwin.selected_point1 = None
				
				mwin.window.itemconfig(temp_point2.tk_id, fill="red")
				temp_point2.color = "red"
				mwin.selected_point2 = None
				
				sprout_draw_all(mwin)
				mwin.master.update()
			
			if mwin.selected_point1 != None and mwin.selected_point2 != None:
				temp_point1 = mwin.selected_point1
				temp_point2 = mwin.selected_point2
				
				connections_found = 0
				p1_found = 0
				p2_found = 0
				
				for c in mwin.connections:
					if temp_point1 == c[0]:
						p1_found += 1
						if temp_point2 == c[1]:
							connections_found += 0
					if temp_point2 == c[0]:
						p2_found += 1
						if temp_point1 == c[1]:
							connections_found += 0
					
					if temp_point1 == c[1]:
						p1_found += 1
					if temp_point2 == c[1]:
						p2_found += 1
				
				for g in mwin.go_around:
					if temp_point1 == g[0]:
						if temp_point2 == g[1]:
							connections_found += 1
					if temp_point2 == g[0]:
						if temp_point1 == g[1]:
							connections_found += 1
				
				if connections_found < 3 and p1_found < 3 and p2_found < 3:
					if (mwin.tree[0] == "B" and mwin.tree[1] == 1 and mwin.tree[2] == "top" or\
					mwin.tree[0] == "C" and mwin.tree[1] == 2 and mwin.tree[2] == "top") and\
					(temp_point1 == mwin.verticies[0] and temp_point2 == mwin.verticies[2] or\
					temp_point2 == mwin.verticies[0] and temp_point1 == mwin.verticies[2]) or\
					mwin.tree[0] == "B" and mwin.tree[1] == 1 and mwin.tree[2] == "bot" and\
					(temp_point1 == mwin.verticies[1] and temp_point2 == mwin.verticies[2] or\
					temp_point2 == mwin.verticies[1] and temp_point1 == mwin.verticies[2]) or\
					(mwin.tree[0] == "C" and mwin.tree[1] == 2 and mwin.tree[2] == "top" and
					(temp_point1 == mwin.verticies[1] and temp_point2 == mwin.verticies[3] or\
					temp_point2 == mwin.verticies[1] and temp_point1 == mwin.verticies[3])):
						
						mwin.offer_active = True
						
						if mwin.tree[2] == "top" and not (mwin.tree[0] == "C" and\
						mwin.tree[1] == 2 and mwin.tree[2] == "top" and
						(temp_point1 == mwin.verticies[1] and temp_point2 == mwin.verticies[3] or\
						temp_point2 == mwin.verticies[1] and temp_point1 == mwin.verticies[3])):
							o_point1 = Sprout_Point(500, 150)
							o_point2 = Sprout_Point(570, 150)
						else:
							o_point1 = Sprout_Point(500, 350)
							o_point2 = Sprout_Point(570, 350)
							
						o_point1.color = "pink"
						o_point2.color = "pink"
						o_point1.is_vertex = True
						o_point2.is_vertex = True
						mwin.o_choices.append(o_point1)
						mwin.o_choices.append(o_point2)
						sprout_draw_shape(o_point1, mwin)
						sprout_draw_shape(o_point2, mwin)
						
						while mwin.offer_active:	
							sprout_draw_all(mwin)
							mwin.master.update()
						
						mwin.window.delete(o_point1.tk_id)
						mwin.window.delete(o_point2.tk_id)
						
						#print mwin.o_chosen, " chosen"
						
						if mwin.o_chosen != None: 
							new_vertex = connect_points(mwin, temp_point1, temp_point2, 1, mwin.o_chosen)
						else:
							continue
						if new_vertex:
							update_level(mwin, temp_point1, temp_point2, mwin.o_chosen)
						
						mwin.o_chosen = None
					
					elif (mwin.tree[0] == "D" and (mwin.tree[1] == 2 or mwin.tree[1] == 3) and
					temp_point1 != mwin.verticies[3] and temp_point2 != mwin.verticies[3]):
						
						mwin.offer_active = True
						
						if mwin.tree[2] == "bot":
							o_point1 = Sprout_Point(500, 150)
							o_point2 = Sprout_Point(570, 150)
						else:
							o_point1 = Sprout_Point(500, 350)
							o_point2 = Sprout_Point(570, 350)
							
						o_point1.color = "pink"
						o_point2.color = "pink"
						o_point1.is_vertex = True
						o_point2.is_vertex = True
						mwin.o_choices.append(o_point1)
						mwin.o_choices.append(o_point2)
						sprout_draw_shape(o_point1, mwin)
						sprout_draw_shape(o_point2, mwin)
						
						while mwin.offer_active:	
							sprout_draw_all(mwin)
							mwin.master.update()
						
						mwin.window.delete(o_point1.tk_id)
						mwin.window.delete(o_point2.tk_id)
						
						#print mwin.o_chosen, " chosen"
						
						if mwin.o_chosen != None: 
							new_vertex = connect_points(mwin, temp_point1, temp_point2, 1, mwin.o_chosen)
						else:
							continue
						if new_vertex:
							update_level(mwin, temp_point1, temp_point2, mwin.o_chosen)
						
						mwin.o_chosen = None
					
					elif (mwin.tree[0] == "D" and mwin.tree[1] == 7 and mwin.tree[2] == "top" and
					temp_point1 != mwin.verticies[2] and temp_point2 != mwin.verticies[2]) or\
					(mwin.tree[0] == "D" and mwin.tree[1] == 7 and mwin.tree[2] == "bot" and
					temp_point1 != mwin.verticies[1] and temp_point2 != mwin.verticies[1]):
						
						mwin.offer_active = True
						
						if mwin.tree[2] == "top":
							o_point1 = Sprout_Point(520, 350)
							o_point2 = Sprout_Point(550, 400)
						else:
							o_point1 = Sprout_Point(510, 225)
							o_point2 = Sprout_Point(540, 180)
							
						o_point1.color = "pink"
						o_point2.color = "pink"
						o_point1.is_vertex = True
						o_point2.is_vertex = True
						mwin.o_choices.append(o_point1)
						mwin.o_choices.append(o_point2)
						sprout_draw_shape(o_point1, mwin)
						sprout_draw_shape(o_point2, mwin)
						
						while mwin.offer_active:	
							sprout_draw_all(mwin)
							mwin.master.update()
						
						mwin.window.delete(o_point1.tk_id)
						mwin.window.delete(o_point2.tk_id)
						
						#print mwin.o_chosen, " chosen"
						
						if mwin.o_chosen != None: 
							new_vertex = connect_points(mwin, temp_point1, temp_point2, 1, mwin.o_chosen)
						else:
							continue
						if new_vertex:
							update_level(mwin, temp_point1, temp_point2, mwin.o_chosen)
						
						mwin.o_chosen = None
					
					elif mwin.tree[0] == "B" and mwin.tree[1] == 1 and mwin.tree[2] == "top" and\
						(temp_point1 == mwin.verticies[1] and temp_point2 == mwin.verticies[2] or\
						temp_point2 == mwin.verticies[1] and temp_point1 == mwin.verticies[2]) or\
						mwin.tree[0] == "B" and mwin.tree[1] == 1 and mwin.tree[2] == "bot" and\
						(temp_point1 == mwin.verticies[0] and temp_point2 == mwin.verticies[1] or\
						temp_point2 == mwin.verticies[0] and temp_point1 == mwin.verticies[1]):
						
						mwin.window.itemconfig(temp_point1.tk_id, fill="red")
						mwin.window.itemconfig(temp_point2.tk_id, fill="red")
						temp_point1.color = "red"
						temp_point2.color = "red"
						
						if mwin.tree[2] == "top":
							temp_point1 = mwin.verticies[0]
							temp_point2 = mwin.verticies[1]
						else:
							temp_point1 = mwin.verticies[0]
							temp_point2 = mwin.verticies[2]
						
						new_vertex = connect_points(mwin, temp_point1, temp_point2, connections_found + 1)
						if new_vertex:
							update_level(mwin, temp_point1, temp_point2)
					
					elif mwin.tree[0] == "C" and mwin.tree[1] == 2 and\
						(temp_point1 == mwin.verticies[1] and temp_point2 == mwin.verticies[0] or\
						temp_point1 == mwin.verticies[0] and temp_point2 == mwin.verticies[1] or\
						temp_point1 == mwin.verticies[3] and temp_point2 == mwin.verticies[2] or\
						temp_point1 == mwin.verticies[2] and temp_point2 == mwin.verticies[3]):
						
						mwin.window.itemconfig(temp_point1.tk_id, fill="red")
						mwin.window.itemconfig(temp_point2.tk_id, fill="red")
						temp_point1.color = "red"
						temp_point2.color = "red"
						
						temp_point1 = mwin.verticies[0]
						temp_point2 = mwin.verticies[3]
						
						new_vertex = connect_points(mwin, temp_point1, temp_point2, connections_found + 1)
						if new_vertex:
							update_level(mwin, temp_point1, temp_point2)
						
						
					elif mwin.tree[0] == "D" and mwin.tree[1] == 8:
						if mwin.tree[2] == "top" and\
						(temp_point1 == mwin.verticies[3] or temp_point2 == mwin.verticies[3]) or\
						mwin.tree[2] == "bot" and\
						(temp_point1 == mwin.verticies[3] or temp_point2 == mwin.verticies[3]):
							
							mwin.window.itemconfig(temp_point1.tk_id, fill="red")
							mwin.window.itemconfig(temp_point2.tk_id, fill="red")
							temp_point1.color = "red"
							temp_point2.color = "red"
							
							if mwin.tree[2] == "top":
								temp_point1 = mwin.verticies[1]
								temp_point2 = mwin.verticies[3]
							else:
								temp_point1 = mwin.verticies[0]
								temp_point2 = mwin.verticies[3]
							
							new_vertex = connect_points(mwin, temp_point1, temp_point2, connections_found + 1)
							if new_vertex:
								update_level(mwin, temp_point1, temp_point2)
						
						else:
							mwin.offer_active = True
							
							if mwin.tree[2] == "top":
								o_point1 = Sprout_Point(465, 317)
								o_point2 = Sprout_Point(450, 450)
							else:
								o_point1 = Sprout_Point(470, 274)
								o_point2 = Sprout_Point(450, 150)
							
							o_point1.color = "pink"
							o_point2.color = "pink"
							o_point1.is_vertex = True
							o_point2.is_vertex = True
							mwin.o_choices.append(o_point1)
							mwin.o_choices.append(o_point2)
							sprout_draw_shape(o_point1, mwin)
							sprout_draw_shape(o_point2, mwin)
							
							while mwin.offer_active:	
								sprout_draw_all(mwin)
								mwin.master.update()
							
							mwin.window.delete(o_point1.tk_id)
							mwin.window.delete(o_point2.tk_id)
							
							#print mwin.o_chosen, " chosen"
							
							if mwin.o_chosen != None: 
								new_vertex = connect_points(mwin, temp_point1, temp_point2, 1, mwin.o_chosen)
							else:
								continue
							if new_vertex:
								update_level(mwin, temp_point1, temp_point2, mwin.o_chosen)
							
							mwin.o_chosen = None
							
					elif mwin.tree[0] == "D" and mwin.tree[1] == 10:
						if temp_point1 == mwin.verticies[4] or temp_point2 == mwin.verticies[4]:
							
							mwin.window.itemconfig(temp_point1.tk_id, fill="red")
							mwin.window.itemconfig(temp_point2.tk_id, fill="red")
							temp_point1.color = "red"
							temp_point2.color = "red"
							
							temp_point1 = mwin.verticies[1]
							temp_point2 = mwin.verticies[4]
							
							new_vertex = connect_points(mwin, temp_point1, temp_point2, connections_found + 1)
							if new_vertex:
								update_level(mwin, temp_point1, temp_point2)
						
						else:
							mwin.offer_active = True
							
							o_point1 = Sprout_Point(535, 321)
							o_point2 = Sprout_Point(550, 450)
							
							o_point1.color = "pink"
							o_point2.color = "pink"
							o_point1.is_vertex = True
							o_point2.is_vertex = True
							mwin.o_choices.append(o_point1)
							mwin.o_choices.append(o_point2)
							sprout_draw_shape(o_point1, mwin)
							sprout_draw_shape(o_point2, mwin)
							
							while mwin.offer_active:	
								sprout_draw_all(mwin)
								mwin.master.update()
							
							mwin.window.delete(o_point1.tk_id)
							mwin.window.delete(o_point2.tk_id)
							
							#print mwin.o_chosen, " chosen"
							
							if mwin.o_chosen != None: 
								new_vertex = connect_points(mwin, temp_point1, temp_point2, 1, mwin.o_chosen)
							else:
								continue
							if new_vertex:
								update_level(mwin, temp_point1, temp_point2, mwin.o_chosen)
							
							mwin.o_chosen = None
							
					elif mwin.tree[0] == "C" and mwin.tree[1] == 5 and\
					(temp_point1 != temp_point2):
						
						mwin.window.itemconfig(temp_point1.tk_id, fill="red")
						mwin.window.itemconfig(temp_point2.tk_id, fill="red")
						temp_point1.color = "red"
						temp_point2.color = "red"
						
						temp_point1 = mwin.verticies[0]
						temp_point2 = mwin.verticies[1]
						
						new_vertex = connect_points(mwin, temp_point1, temp_point2, connections_found + 1)
						if new_vertex:
							update_level(mwin, temp_point1, temp_point2)
						
						
					elif mwin.tree[0] == "C" and mwin.tree[1] == 6 and\
						not (temp_point1 == mwin.verticies[0] and temp_point2 == mwin.verticies[1] or\
							 temp_point1 == mwin.verticies[1] and temp_point2 == mwin.verticies[0] or\
							 temp_point1 == mwin.verticies[2] and temp_point2 == mwin.verticies[3] or\
							 temp_point1 == mwin.verticies[3] and temp_point2 == mwin.verticies[2] ):
						
						mwin.window.itemconfig(temp_point1.tk_id, fill="red")
						mwin.window.itemconfig(temp_point2.tk_id, fill="red")
						temp_point1.color = "red"
						temp_point2.color = "red"
						
						temp_point1 = mwin.verticies[0]
						temp_point2 = mwin.verticies[2]
						
						new_vertex = connect_points(mwin, temp_point1, temp_point2, connections_found + 1)
						if new_vertex:
							update_level(mwin, temp_point1, temp_point2)
						
						
					elif not (temp_point1 == temp_point2):
						#print "p1 = ", temp_point1
						#print "p2 = ", temp_point2
						
						new_vertex = connect_points(mwin, temp_point1, temp_point2, connections_found + 1)
						if new_vertex:
							update_level(mwin, temp_point1, temp_point2)
						
						#print "connections = ", mwin.connections
					
					else:
						if (p1_found != 2):
							#print "p1 = ", temp_point1
							#print "p2 = ", temp_point2
					
							new_vertex = connect_points(mwin, temp_point1, temp_point2, connections_found + 1)
							if new_vertex:
								update_level(mwin, temp_point1, temp_point2)
						
							#print "connections = ", mwin.connections
					
				mwin.window.itemconfig(temp_point1.tk_id, fill="red")
				temp_point1.color = "red"
				mwin.selected_point1 = None
				
				mwin.window.itemconfig(temp_point2.tk_id, fill="red")
				temp_point2.color = "red"
				mwin.selected_point2 = None
				
			try:	
				sprout_draw_all(mwin)
				mwin.master.update()
			except:
				quit()
			

mwin.master.after(500, main)
mainloop()